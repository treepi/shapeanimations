//
//  Utility.swift
//  ShapeAnimations
//
//  Created by k ely on 2/26/15.
//  Copyright (c) 2015 flyingfresh. All rights reserved.
//

import Foundation
import UIKit

let values = [0, 10, -10, 10, 0 ]
let reverseValues = [0, -10, 10, 0]
let keyTimes = [0, (1 / 6.0), (3 / 6.0), (5 / 6.0), 1]
let duration = 0.4
let timingFunction = CAMediaTimingFunction(controlPoints: 0.5,0,0.9,0.7)
let calculationMode = kCAAnimationLinear
let fillMode = kCAFillModeForwards
let radius = (UIScreen.mainScreen().bounds.size.width - radiusOffset) / 2
let radiusOffset: CGFloat = 50

enum Theme {
  case Light, Dark
}

func randomFromZeroTo(max: CGFloat) -> CGFloat {
  return CGFloat(Float(arc4random()) / Float(UInt32.max)) * max
}

extension Double {
  var radians: CGFloat {
    let degreeToRadiansFloatValue = self * M_PI / 180
    return CGFloat(degreeToRadiansFloatValue)
  }
}

// calculate polygon points from view's center, this is similar approach to radar chart algorithm
func calculateAxisPoints(#axisScores: [CGFloat], inFrame frame: CGRect) -> [CGPoint] {
  var angle = 0.radians
  var points = [CGPoint]()
  for score in axisScores {
    let adjacent = cos(angle) * radius * score / PolygonSetting.maxValue
    let opposite = sin(angle) * radius * score / PolygonSetting.maxValue
    let point: CGPoint = CGPoint(x: (frame.size.width / 2.0) + adjacent, y: (frame.size.height / 2.0) + opposite)
    
    points.append(point)
    angle += (Double(360) / Double(PolygonSetting.numberOfAxes)).radians
  }
  return points
}

func animationWithOptions<T: NSObject> (
  #keyPath: String,
  duration: Double? = nil,
  fromValue: T? = nil,
  toValue: T? = nil,
  byValue: T? = nil,
  timingFunction: CAMediaTimingFunction? = nil,
  fillMode: String? = nil,
  removedOnCompletion: Bool? = nil,
  beginTime: NSTimeInterval? = nil,
  delay: NSTimeInterval? = nil,
  repeatCount: Float? = nil,
  autoreverses: Bool? = nil,
  speed: Float? = nil)  -> CABasicAnimation {
    
    let animation = CABasicAnimation(keyPath: keyPath)
    if let duration = duration {
      animation.duration = duration
    }
    if let fromValue = fromValue {
      animation.fromValue = fromValue
    }
    if let toValue = toValue {
      animation.toValue = toValue
    }
    if let byValue = byValue {
      animation.byValue = byValue
    }
    if let timingFunction = timingFunction {
      animation.timingFunction = timingFunction
    }
    if let fillMode = fillMode {
      animation.fillMode = fillMode
    }
    if let removedOnCompletion = removedOnCompletion {
      animation.removedOnCompletion = removedOnCompletion
    }
    if let beginTime = beginTime {
      animation.beginTime = beginTime
    }
    if let delay = delay {
      animation.beginTime = CACurrentMediaTime() + delay
    }
    if let repeatCount = repeatCount {
      animation.repeatCount = repeatCount
    }
    if let autoreverses = autoreverses {
      animation.autoreverses = autoreverses
    }
    if let speed = speed {
      animation.speed = speed
    }
    return animation
}

func keyFrameAnimationWithOptions<T: NSObject>(
  #keyPath: String,
  duration: Double? = nil,
  values: [T]? = nil,
  keyTimes: [T]? = nil,
  calculationMode: String? = nil,
  timingFunction: CAMediaTimingFunction? = nil,
  timingFunctions: [CAMediaTimingFunction]? = nil,
  path: CGPath? = nil,
  fillMode: String? = nil,
  removedOnCompletion: Bool? = nil,
  beginTime: NSTimeInterval? = nil,
  delay: NSTimeInterval? = nil,
  additive: Bool? = nil,
  repeatCount: Float? = nil,
  rotationMode: String? = nil,
  speed: Float? = nil) -> CAKeyframeAnimation {
    
    let animation = CAKeyframeAnimation(keyPath: keyPath)
    if let duration = duration {
      animation.duration = duration
    }
    if let values = values {
      animation.values = values
    }
    if let keyTimes = keyTimes {
      animation.keyTimes = keyTimes
    }
    if let calculationMode = calculationMode {
      animation.calculationMode = calculationMode
    }
    if let timingFunction = timingFunction {
      animation.timingFunction = timingFunction
    }
    if let timingFunctions = timingFunctions {
      animation.timingFunctions = timingFunctions
    }
    if let path = path {
      animation.path = path
    }
    if let fillMode = fillMode {
      animation.fillMode = fillMode
    }
    if let removedOnCompletion = removedOnCompletion {
      animation.removedOnCompletion = removedOnCompletion
    }
    if let beginTime = beginTime {
      animation.beginTime = beginTime + CACurrentMediaTime()
    }
    if let delay = delay {
      animation.beginTime = CACurrentMediaTime() + delay
    }
    if let additive = additive {
      animation.additive = additive
    }
    if let repeatCount = repeatCount {
      animation.repeatCount = repeatCount
    }
    if let rotationMode = rotationMode {
      animation.rotationMode = rotationMode
    }
    if let speed = speed {
      animation.speed = speed
    }
    return animation
}



struct CircleSetting {
  static var theme: Theme = Theme.Light
  static var opacity: Float {
    switch theme {
    case .Light:
      return 0.8
    case .Dark:
      return 0.8
    } }
  static var lineWidth: CGFloat {
    switch theme {
    case .Light:
      return 7.0
    case .Dark:
      return 7.0
    } }
  static var strokeColor: UIColor {
    switch theme {
    case .Light:
      return UIColor.blueColor()
    case .Dark:
      return UIColor.whiteColor()
    } }
  static var fillColor: UIColor {
    switch theme {
    case .Light:
      return UIColor.blackColor()
    case .Dark:
      return UIColor.redColor()
    } }
}

struct PolygonSetting {
  static var theme: Theme = Theme.Light
  static var numberOfAxes: Int = 50
  static var maxValue: CGFloat = 100
  static var lineJoin = kCALineJoinRound
  static var opacity: Float {
    switch theme {
    case .Light:
      return 0.8
    case .Dark:
      return 0.8
    } }
  static var lineWidth: CGFloat {
    switch theme {
    case .Light:
      return 5.0
    case .Dark:
      return 5.0
    } }
  static var strokeColor: UIColor {
    switch theme {
    case .Light:
      return UIColor.blueColor()
    case .Dark:
      return UIColor.whiteColor()
    } }
  static var fillColor: UIColor {
    switch theme {
    case .Light:
      return UIColor.clearColor()
    case .Dark:
      return UIColor.clearColor()
    } }
}
