//
//  ViewController.swift
//  ShapeAnimations
//
//  Created by k ely on 2/26/15.
//  Copyright (c) 2015 flyingfresh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  var circleLayer = CAShapeLayer()
  var polygonLayer = CAShapeLayer()
  
  @IBOutlet weak var imageView: UIImageView!
  
  @IBAction func theme1ButtonTapped(sender: UIButton) {
    view.backgroundColor = UIColor.lightGrayColor()
    CircleSetting.theme = Theme.Light
    PolygonSetting.theme = Theme.Light
    circleLayer.removeAllAnimations()
    polygonLayer.removeAllAnimations()
    removeSublayers()
    setupCircleLayer()
    setupPolygonLayer()
    animateLayers()
  }
  
  @IBAction func theme2ButtonTapped(sender: UIButton) {
    view.backgroundColor = UIColor.blackColor()
    CircleSetting.theme = Theme.Dark
    PolygonSetting.theme = Theme.Dark
    circleLayer.removeAllAnimations()
    polygonLayer.removeAllAnimations()
    removeSublayers()
    setupCircleLayer()
    setupPolygonLayer()
    animateLayers()
  }
  
  // setup circle shape layer
  func setupCircleLayer() {
    let circlePath = UIBezierPath(arcCenter: view.center, radius: radius, startAngle: 0.radians, endAngle: 360.radians, clockwise: true)
    circleLayer.name = "CircleLayer"
    circleLayer.path = circlePath.CGPath
    circleLayer.opacity = CircleSetting.opacity
    circleLayer.lineWidth = CircleSetting.lineWidth
    circleLayer.strokeColor = CircleSetting.strokeColor.CGColor
    circleLayer.fillColor = CircleSetting.fillColor.CGColor
    view.layer.addSublayer(circleLayer)
  }
  
  // setup a polygon based on scores equal set of scores result in regular polygon e. g. scoresArray = [100, 100, 100, 100, 100, 100, 100, 100], however lenght of each axes may vary depending on scores array resulting in rather more irregular shape polygon
  func setupPolygonLayer() {
    var scoresArray: [CGFloat] = []
    for i in 1...PolygonSetting.numberOfAxes{
      scoresArray.append(randomFromZeroTo(100))
    }
    let points = calculateAxisPoints(axisScores: scoresArray, inFrame: view.frame)
    let polygonPath = UIBezierPath()
    polygonPath.moveToPoint(points[0])
    for point in points {
      polygonPath.addLineToPoint(point)
    }
    polygonPath.closePath()
    polygonLayer.path = polygonPath.CGPath
    polygonLayer.name = "PolygonLayer"
    polygonLayer.opacity = PolygonSetting.opacity
    polygonLayer.lineWidth = PolygonSetting.lineWidth
    polygonLayer.strokeColor = PolygonSetting.strokeColor.CGColor
    polygonLayer.fillColor = PolygonSetting.fillColor.CGColor
    polygonLayer.lineJoin = PolygonSetting.lineJoin
    view.layer.addSublayer(polygonLayer)
  }
  
  func animateLayers() {
    
    let polygonStrokeAnimation = animationWithOptions(keyPath: "strokeEnd", duration: 2.0, fromValue: 0.0, toValue: 1.0, timingFunction: timingFunction)
    let polygonShakeAnimation = keyFrameAnimationWithOptions(keyPath: "position.x", duration: 1.0, values: values, timingFunction: CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut), repeatCount: 5, delay: 2.0)
    let circleStrokeAnimation = animationWithOptions(keyPath: "strokeEnd", duration: 2.0, fromValue: 0.0, toValue: 1.0, timingFunction: CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut))
    let circleShakeAnimation = keyFrameAnimationWithOptions(keyPath: "position.y", duration: 1.0, values: reverseValues, calculationMode: kCAAnimationPaced, repeatCount: 5, delay: 2.0)
    
    polygonLayer.addAnimation(polygonStrokeAnimation, forKey: "PolygonStroke")
    polygonLayer.addAnimation(polygonShakeAnimation, forKey: "PolygonShake")
    circleLayer.addAnimation(circleStrokeAnimation, forKey: "CircleStroke")
    circleLayer.addAnimation(circleShakeAnimation, forKey: "CircleShake")
  }
  
  func removeSublayers() {
    if let sublayers = view.layer.sublayers {
      for sublayer in sublayers {
        if sublayer is CAShapeLayer {
          sublayer.removeFromSuperlayer()
          println("\(sublayer.name) was deleted!")
        }
      }
    }
  }
}